import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:dotify/views/pages/artist_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';

class ArtistCard extends StatelessWidget {
  final OnlineUser onlineArtist;

  const ArtistCard({Key key,@required this.onlineArtist}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {

      return  Padding(
      padding: const EdgeInsets.only(left: 10 , right: 10),
      child: Column(
        children: <Widget>[
          InkWell(
              onTap: () {
                 Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return ArtistPage(onlineArtist: onlineArtist,);
                        }));
              },
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImageWithRetry(model.getUserAvatar(userId: onlineArtist.id))
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        onlineArtist.name,
                      ),
                    ),
                  ),
                  Spacer(),
                 
                     IconButton(iconSize: 30,
                       icon: Icon(Icons.more_vert , color: Colors.black45,), onPressed: () {},
                    ),
                  
                ],
              )),
          Divider(
            indent: 10,
            color: Colors.black26,
          )
        ],
      ),
    );
    },);
    
   
  }
}
