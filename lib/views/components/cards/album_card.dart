import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_album_model.dart';
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:dotify/views/pages/album_page.dart';

import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';

class AlbumCard extends StatelessWidget {
  final OnlineAlbum onlineAlbum;
  final OnlineUser onlineArtist;


  const AlbumCard({
    Key key,
    @required this.onlineAlbum, this.onlineArtist,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    
    double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Column(
          children: <Widget>[
             InkWell(
                onTap: () {
                  
                    Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return AlbumPage(
                            onlineAlbum: onlineAlbum,
                           
                          );
                        }));
                },
                child: ListTile(
                    leading: Container(
                        height: 40,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(
                              image: NetworkImageWithRetry(
                                  model.getAlbumCover(albumId: onlineAlbum.id, )),
                              fit: BoxFit.cover),
                        )),
                    title: Text(onlineAlbum.name),
                    subtitle: Text(onlineAlbum.userId.toString()),
                    trailing: IconButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return AlbumPage(
                            onlineAlbum: onlineAlbum,
                           
                          );
                        }));
                      },
                      icon: Image.asset(
                        "assets/icons/dotify/view_album.png",
                        height: height / 45,
                      ),
                    )),
              ),
            
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Divider(endIndent: 30, color: Colors.black26),
            )
          ],
        );
      },
    );
  }
}
