import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_playlist_model.dart';
import 'package:dotify/views/pages/playlist_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';

class PlayListCard extends StatelessWidget {
  final OnlinePlaylist playlist;

  const PlayListCard({
    Key key,
    @required this.playlist,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      return InkWell(
        onTap: () {
          print(model.idOfSongToAdd);
          model.addSongToPlayList
              ? model.addToPlaylist(model.idOfSongToAdd, playlist.id)
              : null;
         
          model.addSongToPlayList
              ? Navigator.pop(context)
              : Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                  return PlaylistsScreen(
                    playlist: playlist,
                  );
                }));

          if (model.addSongToPlayList == true) {
            model.doAddSongToPlaylist();
          }
        },
        child: Card(
            child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImageWithRetry(
                          model.getPlaylistCover(playlistId: playlist.id)),
                      fit: BoxFit.cover)),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.black12.withOpacity(0.03),
                child: Column(
                  children: <Widget>[
                    Text(
                      "",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    Text(
                      playlist.name,
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    )
                  ],
                ),
              ),
            )
          ],
        )),
      );
    });
  }
}
