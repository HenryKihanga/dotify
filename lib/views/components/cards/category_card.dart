
import 'package:dotify/views/components/models/song_category_model.dart';
import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final SongCategory songCategory;

  const CategoryCard({Key key, @required this.songCategory})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    //double height = MediaQuery.of(context).size.height;
    //double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Card(
        child: Container(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 134,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(songCategory.cover),
                            fit: BoxFit.cover)),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 100,
                      height: 100,
                      color: Colors.black12,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          Text(
                            songCategory.name,
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(songCategory.name),
              )
            ],
          ),
        ),
      ),
    );
  }
}
