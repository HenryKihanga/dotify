
import 'package:dotify/views/components/models/song_genre_model.dart';
import 'package:flutter/material.dart';



class GenresCard extends StatelessWidget {
  final SongGenres songGenres;

  const GenresCard({Key key, @required this.songGenres})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    //double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.all(1),
      child: Card(
        child: Container(
          color: Colors.white,
          height: 180,
          width: 140,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Container(
                      height: 130,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(songGenres.cover),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: Container(
                      height: 130,
                      color: Color(0x75E55F2C),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:50 , left: 45 ),
                    child: Image(
                      image: AssetImage(songGenres.icon),
                      height: 50,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(songGenres.name),
              )
            ],
          ),
        ),
      ),
    );
  }
}
