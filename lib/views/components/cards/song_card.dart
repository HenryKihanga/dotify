import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:dotify/views/components/resources/bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class SongCard extends StatelessWidget {
  final OnlineSong song;

  const SongCard({Key key,@required this.song}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Column(
          children: <Widget>[
            InkWell(
              onTap: () {
              
                model.play(songId: song.id);
                //set is played
                model.setIsPlayed(true);
                //set the id of playing song
                model.setNowPlayingSongId(songId: song.id);
              

              },
              child: ListTile(
                leading: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    
                    Icons.music_note
                  ),
                ),
                title: Text(song.title),
                subtitle: Text(song.viewers.toString()),
                trailing: InkWell(
                    onTap: () {
                    
                       _showMore(context);
                      
                    },
                    child: Image(
                      image: AssetImage("assets/icons/dotify/more_info.png"),
                      height: 25,
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Divider(
                color: Colors.black26,
                endIndent: 20,
              ),
            )
          ],
        );
      },
    );
  }

  void _showMore(BuildContext context) {
    showModalBottomSheet(
        builder: (BuildContext context) {

          return MyBottomSheet(song: song);
        },
        context: context);
  }
}
