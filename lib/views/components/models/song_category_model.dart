
import 'package:flutter/material.dart';

class SongCategory{
  final String cover;
  final String name;

  SongCategory({@required this.cover,@required this.name});

}
List<SongCategory> songcategories = [
  SongCategory(cover: "assets/images/dotify/20.png", name: "TOP HITS"),
  SongCategory(cover: "assets/images/dotify/14.png", name: "VIRAL"),
  SongCategory(cover: "assets/images/dotify/4.png", name: "MOST PLAYED"),
  SongCategory(cover: "assets/images/dotify/15.png", name: "RECENTLY"),
  SongCategory(cover: "assets/images/dotify/20.png", name: "POPULAR"),
];