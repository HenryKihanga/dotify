import 'package:flutter/widgets.dart';

class OnlineRole {
  final int id;
  final String name;
  final String description;

  OnlineRole(
      {@required this.id, @required this.name, @required this.description});

  OnlineRole.frommap(Map<String, dynamic> map)
      : assert(map['id'] != null),
        assert(map['name'] != null),
        id = map['id'],
        name = map['name'],
        description = map['description'];
}
