import 'package:dotify/views/components/models/online_song_model.dart';

class OnlineAlbum {
  final int id;
  final int userId;
  final String name;
  final String coverFile;
  final String pathToFile;
  final bool isDefault;
  List<OnlineSong> songs =[] ;

  OnlineAlbum(this.id, this.userId, this.name, this.coverFile, this.pathToFile,
      this.songs, this.isDefault);

  OnlineAlbum.fromMap(Map<String, dynamic> map)
      : assert(map['id'] != null),
        assert(map['user_id'] != null),
        assert(map['name'] != null),
        assert(map['cover_file'] != null),
        assert(map['path_to_file'] != null),
        assert(map['is_default'] != null),
        
        id = map['id'],
        userId = map['user_id'],
        name = map['name'],
        coverFile = map['cover_file'],
        pathToFile = map['path_to_file'],
        isDefault = map['is_default'] == 1 ? true : false;
        
}
