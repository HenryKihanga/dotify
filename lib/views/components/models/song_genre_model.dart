import 'package:flutter/material.dart';

class SongGenres {
  final String cover;
  final String name;
  final String icon;

  SongGenres({@required this.icon, @required this.cover, @required this.name});
}

List<SongGenres> songgenres = [
  SongGenres(
      cover: "assets/images/dotify/1.png",
      name: "Pop",
      icon: "assets/icons/dotify/pop_mic.png"),
  SongGenres(
      cover: "assets/images/dotify/19.png",
      name: "Rock",
      icon: "assets/icons/dotify/rock_fire.png"),
  SongGenres(
      cover: "assets/images/dotify/18.png",
      name: "Rege",
      icon: "assets/icons/dotify/radio.png"),
  SongGenres(
      cover: "assets/images/dotify/15.png",
      name: "Bongo",
      icon: "assets/icons/dotify/pop_mic.png"),
  SongGenres(
      cover: "assets/images/dotify/20.png",
      name: "HipHap",
      icon: "assets/icons/dotify/rock_fire.png"),
];
