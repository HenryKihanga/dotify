class OnlineSong {
  final int id;
  final String title;
  final int viewers;
  final String songFile;
  final String coverFile;
  final int albumId;
  final int genreId;
  final bool isTopHit;
  final bool isViral;
  final bool isChillAcoustic;

  OnlineSong(
      this.id,
      this.title,
      this.viewers,
      this.songFile,
      this.coverFile,
      this.albumId,
      this.genreId,
      this.isTopHit,
      this.isViral,
      this.isChillAcoustic);

  OnlineSong.fromMap(Map<String , dynamic> map):
  assert(map['id']!=null),
  assert(map['genre_id']!=null),
  assert(map['album_id']!=null),
  assert(map['title']!=null),
  assert(map['viewers']!=null),
  assert(map['song_file']!=null),
  assert(map['cover_file']!=null),
  id=map['id'],
  title=map['title'],
  viewers=map['viewers'],
  songFile=map['song_file'],
  coverFile=map['cover_file'],
  albumId=map['album_id'],
  genreId=map['genre_id'] ,
  isTopHit=map['is_top_hit'],
  isViral=map['is_viral'],
  isChillAcoustic=map['is_chill_acoustic'];
}
