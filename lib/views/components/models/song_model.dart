import 'package:flutter/material.dart';
//import 'package:ui_challenge/Pages/dotify/components/models/playlist.dart';

class Song {
  final int id;
  final int albumId;
  final int playListId;
  final String name;
  final String views;
  final String cover;
  final List<int> playListIds ;
  

  Song(
      {this.playListId,
      @required this.albumId,
      @required this.cover,
      @required this.views,
      @required this.id,
      @required this.name,
      this.playListIds =const<int>[]
      });
}

List<Song> songs =<Song> [
  Song(
      id: 1,
      name: "I miss you",
      views: "1,466,678",
      albumId: 1,
      cover: "assets/images/dotify/11.png"),
  Song(
      id: 2,
      name: "My Number one Fine",
      views: "4786,789",
      albumId: 1,
      cover: "assets/images/dotify/12.png"),
  Song(
      id: 3,
      name: "Masogange",
      views: "876t7889",
      albumId: 1,
      cover: "assets/images/dotify/13.png"),
  Song(
      id: 4,
      name: "Sumu ya penzi",
      views: "1,657",
      albumId: 2,
    
      cover: "assets/images/dotify/14.png"),
  Song(
      id: 5,
      name: "Matatizo",
      views: "7,897",
      albumId: 2,
      
      cover: "assets/images/dotify/15.png"),
  Song(
      id: 6,
      name: "Uzuri wako",
      views: "1,466,678",
      albumId: 2,
      
      cover: "assets/images/dotify/16.png"),
  Song(
      id: 7,
      name: "Ntampata wapi",
      views: "4786,789",
      albumId: 2,
      
      cover: "assets/images/dotify/17.png"),
  Song(
      id: 8,
      name: "Mbagala",
      views: "876t7889",
      albumId: 3,
      
      cover: "assets/images/dotify/18.png"),
  Song(
      id: 9,
      name: "Niseme nini",
      views: "1,657",
      albumId: 4,
      
      cover: "assets/images/dotify/19.png"),
  Song(
      id: 10,
      name: "Kisura",
      views: "7,897",
      albumId: 5,
      
      cover: "assets/images/dotify/20.png"),
  Song(
      id: 11,
      name: "Mkali wao",
      views: "1,466,678",
      albumId: 6,
      
      cover: "assets/images/dotify/1.png"),
  Song(
      id: 12,
      name: "Bei ya mkaa",
      views: "4786,789",
      albumId: 7,
      
      cover: "assets/images/dotify/2.png"),
  Song(
      id: 13,
      name: "Hasara Roho",
      views: "876t7889",
      albumId: 8,
      cover: "assets/images/dotify/3.png"),
  Song(
      id: 14,
      name: "Kwetu pazuri",
      views: "1,657",
      albumId: 9,
      cover: "assets/images/dotify/4.png"),
  Song(
      id: 15,
      name: "Nuru",
      views: "7,897",
      albumId: 10,
      cover: "assets/images/dotify/5.png"),
  Song(
      id: 16,
      name: "paulo na Sila",
      views: "1,466,678",
      albumId: 11,
      cover: "assets/images/dotify/6.png"),
  Song(
      id: 17,
      name: "Mfalme wa amani",
      views: "4786,789",
      albumId: 12,
      cover: "assets/images/dotify/7.png"),
  Song(
      id: 18,
      name: "Mwenye Haki",
      views: "876t7889",
      albumId: 13,
      cover: "assets/images/dotify/8.png"),
  Song(
      id: 19,
      name: "Nimempata yesu",
      views: "1,657",
      albumId: 14,
      cover: "assets/images/dotify/9.png"),
  Song(
      id: 20,
      name: "Pombe mbaya",
      views: "7,897",
      albumId: 3,
      cover: "assets/images/dotify/10.png"),
  Song(
      id: 21,
      name: "Hasira hasara",
      views: "1,466,678",
      albumId: 4,
      cover: "assets/images/dotify/21.png"),
  Song(
      id: 22,
      name: "Chunga tamaa mbaya",
      views: "4786,789",
      albumId: 5,
      cover: "assets/images/dotify/22.png"),
  Song(
      id: 23,
      name: "Saidia masikin",
      views: "876t7889",
      albumId: 6,
      cover: "assets/images/dotify/23.png"),
  Song(
      id: 24,
      name: "Mwenye heri",
      views: "1,657",
      albumId: 7,
      cover: "assets/images/dotify/24.png"),
  Song(
      id: 25,
      name: "Kiongozi wa kwel",
      views: "7,897",
      albumId: 8,
      cover: "assets/images/dotify/25.png"),
  Song(
      id: 26,
      name: "Uta nitambuaje",
      views: "1,466,678",
      albumId: 9,
      cover: "assets/images/dotify/26.png"),
  Song(
      id: 27,
      name: "Mama ni mama",
      views: "4786,789",
      albumId: 10,
      cover: "assets/images/dotify/27.png"),
  Song(
      id: 28,
      name: "Jongeeni Kalamuni",
      views: "876t7889",
      albumId: 11,
      cover: "assets/images/dotify/28.png"),
  Song(
      id: 29,
      name: "Kwaheri ya kuonana",
      views: "1,657",
      albumId: 12,
      cover: "assets/images/dotify/29.png"),
  Song(
      id: 30,
      name: "Kaolewa",
      views: "7,897",
      albumId: 3,
      cover: "assets/images/dotify/30.png"),
  Song(
      id: 31,
      name: "Kivuruge",
      views: "1,466,678",
      albumId: 4,
      cover: "assets/images/dotify/11.png"),
  Song(
      id: 32,
      name: "Njiwa",
      views: "4786,789",
      albumId: 5,
      cover: "assets/images/dotify/12.png"),
  Song(
      id: 33,
      name: "Ntampata wapi",
      views: "876t7889",
      albumId: 6,
      cover: "assets/images/dotify/13.png"),
  Song(
      id: 34,
      name: "Mbagala",
      views: "1,657",
      albumId: 7,
      cover: "assets/images/dotify/14.png"),
  Song(
      id: 35,
      name: "wewe ni mwema",
      views: "7,897",
      albumId: 8,
      cover: "assets/images/dotify/18.png"),
];
