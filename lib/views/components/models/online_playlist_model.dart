import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:flutter/material.dart';

class OnlinePlaylist{
  final int id;
  final String name;
  final String coverFile;
  final String pathToFile;
  List <OnlineSong> songs=[];

  OnlinePlaylist(  {@required this.id,@required this.name,@required this.coverFile,@required this.pathToFile,});


  OnlinePlaylist.fromMap(Map<String, dynamic> map)
      : assert(map['id'] != null),
        assert(map['name'] != null),
        assert(map['cover_file'] != null),
        assert(map['path_to_file'] != null),
        id = map['id'],
        name = map['name'],
        coverFile = map['cover_file'],
        pathToFile=map['path_to_file'];
        

}
