import 'package:dotify/views/components/models/online_album_model.dart';
import 'package:dotify/views/components/models/online_role.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:flutter/widgets.dart';

class OnlineUser {
  final int id;
  final String name;
  final String email;
  final String avatarFile;
  final String token;
   OnlineRole roles;
   List<OnlineAlbum> albums=[];
   List<OnlineSong> songs=[];

  OnlineUser( 
      {this.roles,
      this.songs,
      this.albums,
      this.avatarFile,
      @required this.token,
      @required this.id,
      @required this.name,
      @required this.email,
      });

  OnlineUser.fromMap(Map<String, dynamic> map)
      : assert(map['id'] != null),
        assert(map['name'] != null),
        assert(map['email'] != null),
        assert(map['token'] != null),
        id = map['id'],
        name = map['name'],
        email = map['email'],
        token=map['token'],
        avatarFile = map['avatar_file'];
        
        

}
