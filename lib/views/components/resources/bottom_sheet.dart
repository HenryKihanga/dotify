import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:dotify/views/pages/list_of_playlists.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:share/share.dart';

class MyBottomSheet extends StatelessWidget {
  final OnlineSong song;

  const MyBottomSheet({Key key,@required this.song}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
            builder: (BuildContext context, Widget child, MainModel model) {
              return Container(
                height: height / 4,
                color: Color(0xfffE55F2C),
                child:SingleChildScrollView (
                                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: InkWell(
                          onTap: () {},
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.play_arrow),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Play")
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: InkWell(
                          onTap: () {
                            model.idOfsongToAdd(songId: song.id);
                             model.doAddSongToPlaylist();
                            Navigator.pop(context);
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context){
                              return ListOfPlayListScreen();
                            }));
                            
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.playlist_add),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Add PlayList")
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: InkWell(
                          onTap: () {
                            Share.share("My Sharing Demo");
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.share),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Share")
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }
}