import 'dart:ui';

import 'package:flutter/material.dart';

class BackGround extends StatelessWidget {
  final Scaffold scaffold;
  final double sigmaX;
  final double sigmaY;

  const BackGround({Key key,@required this.scaffold,@required this.sigmaX,@required this.sigmaY}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/dotify/8.png'),
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.9), BlendMode.saturation),
              fit: BoxFit.cover)),
      child: BackdropFilter(filter: ImageFilter.blur(sigmaX:sigmaX, sigmaY: sigmaY),
      child: scaffold,),
    );
  }
}
