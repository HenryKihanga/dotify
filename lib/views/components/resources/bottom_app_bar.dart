
import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:dotify/views/pages/now_playing_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';


class MyBottomAppBar extends StatelessWidget {
  final OnlineSong song;

 const MyBottomAppBar({Key key,@required this.song,}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return  ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {
      return InkWell(
                onTap: () {
                 Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return NowPlayingPage(song: song,model: model,
                            
                          );
                        }));
                },
                child: Container(
                  height: height / 10,
                  
                 
                     color:  Colors.white70,
                    
                    child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage:
                              NetworkImageWithRetry(model.getSongCover(songId: song.id)),
                          radius: 25,
                        ),
                        title: Text(song.title),
                        subtitle: Text(
                          "Coldplay f",
                          maxLines: 2,
                        ),
                        trailing: model.isPlayed?
                        InkWell(
                          onTap: () {
                            model.pause();
                            model.setIsPlayed(false);
                            //model.showBottomAppBar();
                          },
                          child: Image(
                            image: AssetImage("assets/icons/dotify/pause.png" ),
                            height: 25,
                          ),
                        ): InkWell(
                          onTap: () {
                            model.play(songId: song.id);
                            model.setIsPlayed(true);
                            //model.showBottomAppBar();
                          },
                          child: Image(
                            image: AssetImage("assets/icons/dotify/play_btn_pop_up.png" ),
                            height: 25,
                          ),
                        )),
                 
                ),
              
            );
    },);
    
  }
}