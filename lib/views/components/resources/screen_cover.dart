import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';

class ScreenCover extends StatelessWidget {
  final String screenCover;
  final String screenAvatar;
  final String screenName;
  final String screentitle;

  const ScreenCover({Key key,@required this.screenCover,@required this.screenAvatar,@required this.screenName,@required this.screentitle}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
                  height: height * 5 / 13.3,
                  child: Stack(
                    children: <Widget>[
                      Card(
                        elevation: 16,
                        child: Container(
                          height: height * 1 / 3.2,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImageWithRetry(screenCover), fit: BoxFit.cover)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 50, left: 20, right: 35),
                        child: Container(
                          height: height / 16,
                          child: Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  print("Back to PlayLists");
                                  Navigator.pop(context);
                                },
                                child: Image(
                                  image: AssetImage(
                                      "assets/icons/dotify/back_btn_light.png"),
                                  color: Colors.white,
                                  height: 25,
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        screentitle,
                                        style: TextStyle(
                                            fontSize: 10, color: Colors.grey),
                                      ),
                                      Text(
                                        screenName,
                                        style: TextStyle(
                                            fontSize: 10, color: Colors.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            backgroundImage: NetworkImageWithRetry(screenAvatar),
                            radius: 55,
                          ),
                        ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 200, left: 125),
                          child: Row(
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    "assets/icons/dotify/heart_song.png"),
                                height: 40,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              InkWell(
                                onTap: (){
                                  
                                },
                                                              child: Image(
                                  image: AssetImage(
                                      "assets/icons/dotify/play_artist.png"),
                                  height: 60,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              InkWell(
                                onTap: () {},
                                child: Image(
                                  image: AssetImage(
                                      "assets/icons/dotify/share.png"),
                                  height: 40,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 30),
                            child: Text("TRACK LIST"),
                          ))
                    ],
                  ),
                );
  }
}