import 'package:dotify/constants/constant.dart';
import 'package:dotify/data/main_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Drawer(
          child: Container(
            height: height,
            width: width * 8/9,
            decoration: BoxDecoration(
                color: Color(0xfffE55F2C),
                image: DecorationImage(
                    image: AssetImage("assets/images/dotify/4.png"),
                    fit: BoxFit.cover)),
            child: Container(
              color: Color(0xfffE55F2C).withOpacity(0.8),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: height / 15,
                    ),
                    Container(
                      
                      height: height/20,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Image(
                            image: AssetImage(
                                "assets/icons/dotify/dotify_logo_light.png"),
                            height: height/20,
                          )),
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                            child: Icon(
                              Icons.settings,
                              color: Colors.white,
                              size: 30,
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height / 30,
                    ),
                    CircleAvatar(
                      backgroundImage: NetworkImageWithRetry(model
                          .getUserAvatar(userId: model.authenticatedUser.id)),
                      radius: height / 15,
                    ),
                    SizedBox(
                      height: height / 30,
                    ),
                    Container(
                      height: height/40,
                    
                      child: Text(
                        model.authenticatedUser.name,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: height / 30,
                    ),
                    Padding(
                      padding: EdgeInsets.all(height / 100),
                      child: InkWell(
                        onTap: () {
                          print("Browse clicked");
                          Navigator.pushNamed(context, browsePage);
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 30),
                          height: height / 15,
                          // color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    "assets/icons/dotify/browse.png"),
                                height: 30,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "BROWSE",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(height / 100),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          padding: EdgeInsets.only(left: 30),
                          height: height / 15,
                          // color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    "assets/icons/dotify/activity.png"),
                                height: 30,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "ACTIVITY",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(height / 100),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          padding: EdgeInsets.only(left: 30),
                          height: height / 15, // color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Image(
                                image:
                                    AssetImage("assets/icons/dotify/radio.png"),
                                height: 30,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "RADIO",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(height / 100),
                      child: InkWell(
                        onTap: () {
                          print("YourMusic clicked");
                          Navigator.pushNamed(context, yourMusicPage);
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 30),
                          height: height / 15,
                         
                          child: Row(
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    "assets/icons/dotify/music_library.png"),
                                height: 30,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "YOUR MUSIC",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height *127/600,
                    ),
                    Container(
                      height: height/15,
                      color: Colors.white,
                      width: width,
                      child: FlatButton(
                        color: Colors.white,
                        child: Text(
                          'LOGOUT',
                          style:
                              TextStyle(color: Color(0xffE55F2C), fontSize: 20),
                        ),
                        onPressed: () {
                          model.logout(token: model.authenticatedUser.token);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
