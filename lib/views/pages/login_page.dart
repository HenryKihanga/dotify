import 'package:dotify/constants/constant.dart';
import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/resources/background.dart';
import 'package:dotify/views/pages/browse_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = TextEditingController();

  TextEditingController _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    //double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      return BackGround(
          sigmaX: 15,
          sigmaY: 15,
          scaffold: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.transparent,
            appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                title: Center(
                    child: Image(
                  image: AssetImage("assets/icons/dotify/dotify_icon.png"),
                  height: height / 20,
                )),
                leading: IconButton(
                  onPressed: () {
                    print("back to launch screen");
                    Navigator.pushNamed(context, launchPage);
                  },
                  icon: Image.asset(
                    "assets/icons/dotify/back.png",
                    height: height / 35,
                  ),
                )),
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: height / 15,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton.icon(
                            icon: Icon(
                              FontAwesomeIcons.facebookF,
                              color: Colors.white,
                              size: height / 30,
                            ),
                            label: Expanded(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Log in with Facebook",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            color: Color(0xff334F8D),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(height / 25),
                    child: Text("or"),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Card(
                          child: TextFormField(
                            controller: _usernameController,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                border: InputBorder.none,
                                hintText: 'Username',
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: Colors.black,
                                )),
                            validator: (value) {
                              if (value.isEmpty)
                                return "Username Required";
                              else
                                return null;
                            },
                          ),
                        ),
                        Card(
                          child: TextFormField(
                            controller: _passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                border: InputBorder.none,
                                hintText: 'Password',
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.black,
                                )),
                            validator: (value) {
                              if (value.isEmpty)
                                return "Password Required";
                              else
                                return null;
                            },
                          ),
                        ),
                        FlatButton(
                          child: Text(
                            "Forgot your Password?",
                            style: TextStyle(color: Colors.black38),
                          ),
                          onPressed: () {},
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),

            bottomNavigationBar: BottomAppBar(
                child: Container(
              height: height / 15,
              child: FlatButton(
                color: Color(0xffE55F2C),
                child: Text(
                  "LOG IN",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    print(_usernameController.text);
                    print( _passwordController.text);
                    model
                        .login(
                            name: _usernameController.text,
                            password: _passwordController.text)
                        .then((onValue) {
                      if (onValue) {
                        Navigator.pushReplacementNamed(context, weird);
                      } else {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: ListTile(
                            leading: Icon(Icons.error, color: Colors.red),
                            title: Text('Incorrect Email or Password'),
                            trailing: Icon(Icons.error, color: Colors.red),
                          ),
                          backgroundColor: Color(0x75fE55F2C),
                          duration: Duration(seconds: 5),
                        ));
                      }
                    });
                  }
                },
              ),
            )),
          ));
    });
  }
}
