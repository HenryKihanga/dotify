import 'package:dotify/views/components/cards/category_card.dart';
import 'package:dotify/views/components/cards/genre_card.dart';
import 'package:dotify/views/components/models/song_category_model.dart';
import 'package:dotify/views/components/models/song_genre_model.dart';
// import 'package:dotify/views/components/resources/bottom_app_bar.dart';
import 'package:dotify/views/pages/drawer_page.dart';
import 'package:flutter/material.dart';

class BrowsePage extends StatefulWidget {
  @override
  _BrowsePageState createState() => _BrowsePageState();
}

class _BrowsePageState extends State<BrowsePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        // iconTheme: IconThemeData(color: Colors.black ),
        backgroundColor: Colors.transparent,
        leading: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: IconButton(
            icon: Image.asset(
              'assets/icons/dotify/menu.png',
            ),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            },
          ),
        ),
        elevation: 0,
        title: Center(
            child: Image(
          image: AssetImage("assets/icons/dotify/dotify_icon.png"),
          height: 35,
        )),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: InkWell(
                onTap: () {
                  print("search tapped");
                },
                child: Icon(
                  Icons.search,
                  size: 40,
                  color: Colors.black,
                )),
          ),
        ],
      ),
      drawer: Container(
        width: width * 8 / 9,
        child: Drawer(
          child: DrawerPage(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.only(left: 170),
                  child: Text(
                    "BROWSE",
                    style: TextStyle(fontSize: 18),
                  ),
                )
              ]),
            ),
            SliverToBoxAdapter(
              child: Container(
                height: 180,
                child: ListView.builder(
                  itemCount: songcategories.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        width: 140,
                        child: CategoryCard(
                          songCategory: songcategories[index],
                        ));
                  },
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10),
                  child: Text(
                    "FEATURED ALBUM",
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                Divider(
                  color: Colors.black26,
                  endIndent: 30,
                )
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                ListTile(
                    leading: Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(
                              image: AssetImage("assets/images/dotify/8.png"),
                              fit: BoxFit.cover)),
                    ),
                    title: Text("Variable albums"),
                    subtitle: Text("navigator"),
                    trailing: IconButton(
                      onPressed: () {
                        print("view album");
                        // Navigator.pushNamed(context, launchScreen);
                      },
                      icon: Image.asset(
                        "assets/icons/dotify/view_album.png",
                        height: height / 35,
                      ),
                    )),
                Divider(
                  color: Colors.black26,
                  endIndent: 30,
                ),
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "GENRES & MOODS",
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                Divider(
                  color: Colors.black26,
                  endIndent: 30,
                )
              ]),
            ),
            SliverToBoxAdapter(
              child: Container(
                height: 180,
                child: ListView.builder(
                  itemCount: songgenres.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        width: 140,
                        child: GenresCard(
                          songGenres: songgenres[index],
                        ));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      // bottomNavigationBar: BottomAppBar(

      //   child: MyBottomAppBar()

      //        )
    );
  }
}
