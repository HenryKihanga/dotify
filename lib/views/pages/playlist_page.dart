import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/cards/song_card.dart';
import 'package:dotify/views/components/models/online_playlist_model.dart';
import 'package:dotify/views/components/resources/bottom_app_bar.dart';
import 'package:dotify/views/components/resources/screen_cover.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class PlaylistsScreen extends StatelessWidget {
  final OnlinePlaylist playlist;

  const PlaylistsScreen({Key key, @required this.playlist}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            body: Container(
              child: Column(
                children: <Widget>[
                  //pull page cover
                  ScreenCover(
                    screenName: playlist.name,
                    screenCover:
                        model.getPlaylistCover(playlistId: playlist.id),
                    screenAvatar:
                        model.getPlaylistCover(playlistId: playlist.id),
                    screentitle: "PlayList",
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Divider(
                        color: Colors.black26,
                        endIndent: 20,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    height: height * 1 / 2,
                    child: CustomScrollView(
                      slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            return SongCard(
                              song: playlist.songs[index],
                            );
                          },
                          childCount: playlist.songs.length,
                        ))
                      ],
                    ),
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
                child: MyBottomAppBar(
              song: model.getAvailableSongs()[model.lastPlayedSongId],
            )));
      },
    );
  }
}
