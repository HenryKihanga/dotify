import 'dart:ui';

import 'package:dotify/constants/constant.dart';
import 'package:dotify/views/components/resources/background.dart';
import 'package:flutter/material.dart';

class LaunchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return BackGround(
      sigmaX: 5,
      sigmaY: 5,
      scaffold: Scaffold(
        backgroundColor: Colors.transparent,
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Column(
                  children: <Widget>[
                    SizedBox(
                      height: height / 7,
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 30),
                        height: height / 13,
                        width: width / 2,
                        child: Image(
                          image:
                              AssetImage('assets/icons/dotify/dotify_icon.png'),
                        )),
                    SizedBox(height: height / 4),
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 10, right: width / 3),
                      height: height / 8,
                      width: width,
                      color: Color(0xfffE55F2C),
                      child: Center(
                        child: Text(
                          "YOUR MUSIC",
                          style: TextStyle(
                              fontSize: 40,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 15,  right: width / 2),
                      height: height / 15,
                      width: width,
                      color: Color(0x75fE55F2C),
                      child: Center(
                        child: Text(
                          "Tuned to you.",
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ]),
            )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                color: Colors.white,
                width: width / 2,
                child: FlatButton(
                  child: Text(
                    'LOG IN',
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, loginPage);
                  },
                ),
              ),
              Container(
                width: width / 2,
                color: Color(0xfffE55F2C),
                child: FlatButton(
                  child: Text('SIGN UP',
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                  onPressed: () {
                    Navigator.pushNamed(context, signupPage);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
