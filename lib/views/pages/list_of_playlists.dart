import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/cards/playlist_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scoped_model/scoped_model.dart';

class ListOfPlayListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            appBar: AppBar(
              title: Text("my playlist"),
            ),
            body: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: const EdgeInsets.only(left: 125,right: 125),
                      child: RaisedButton.icon(
                        icon: Icon(FontAwesomeIcons.plus),
                        label: Text("ADD PLAYLIST"),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Color(0xffE55F2C),
                        onPressed: () {},
                      ),
                    )
                  ]),
                ),
                SliverGrid(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext contect, int index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          left: index.isEven ? 10 : 0,
                          right: index.isOdd ? 10 : 0),
                      child: PlayListCard(
                        playlist: model.getAvailablePlaylists()[index],
                      ),
                    );
                  }, childCount: model.getAvailablePlaylists().length),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 1,
                      crossAxisSpacing: 1,
                      childAspectRatio: 0.8),
                ),
              ],
            ));
      },
    );
  }
}
