import 'package:dotify/constants/constant.dart';
import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/cards/album_card.dart';
import 'package:dotify/views/components/cards/artist_card.dart';
import 'package:dotify/views/components/cards/playlist_card.dart';
import 'package:dotify/views/components/cards/song_card.dart';
import 'package:dotify/views/components/resources/bottom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class YourMusicPage extends StatefulWidget {
  final MainModel model;

  const YourMusicPage({Key key, @required this.model}) : super(key: key);
  @override
  _YourMusicPageState createState() => _YourMusicPageState();
}

class _YourMusicPageState extends State<YourMusicPage> {
  @override
  void initState() {
    widget.model.fetchAllAlbums();

    widget.model.fetchAllUsers();
    widget.model.getAvailableArtists();
    widget.model.fetchAllPlaylists();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            backgroundColor: Colors.grey[200],
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(height/12),
              child: AppBar(
                backgroundColor: Colors.white,
                elevation: 1,
                leading: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: InkWell(
                    child: Icon(
                      Icons.menu,
                      color: Colors.black,
                      size: 40,
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, drawerPage);
                    },
                  ),
                ),
                title: Center(
                    child: Text(
                  "YOUR MUSIC",
                  style: TextStyle(color: Colors.black),
                )),
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: InkWell(
                        child:
                            Icon(Icons.search, color: Colors.black, size: 40)),
                  )
                ],
              ),
            ),
            body: Padding(
                padding: const EdgeInsets.only(left: 10),

                //Container with choise buttons
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 10,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: InkWell(
                                    onTap: () {
                                      model.setCurrentListId(listId: 0);
                                    },
                                    child: model.isArtistTapped
                                        ? Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/artistsColored.png"),
                                            height: 50,
                                            width: 70,
                                          )
                                        : Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/artistsColorLess.png"),
                                            height: 50,
                                            width: 70,
                                          ))),
                          ),
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: InkWell(
                                    onTap: () {
                                      model.setCurrentListId(listId: 1);
                                    },
                                    child: Card(
                                      child: model.isAlbumTapped
                                          ? Image(
                                              image: AssetImage(
                                                  "assets/icons/dotify/albumColored.png"),
                                              height: 50,
                                              width: 70,
                                            )
                                          : Image(
                                              image: AssetImage(
                                                  "assets/icons/dotify/albumColorLess.png"),
                                              height: 50,
                                              width: 70,
                                            ),
                                    ))),
                          ),
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: InkWell(
                                    onTap: () {
                                      model.setCurrentListId(listId: 2);
                                    },
                                    child: model.isSongsTapped
                                        ? Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/songsColored.png"),
                                            height: 50,
                                            width: 70,
                                          )
                                        : Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/songsColorsLess.png"),
                                            height: 50,
                                            width: 70,
                                          ))),
                          ),
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: InkWell(
                                    onTap: () {
                                      model.setCurrentListId(listId: 3);
                                    },
                                    child: model.isPlayListTapped
                                        ? Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/playlistsColored.png"),
                                            height: 50,
                                            width: 70,
                                          )
                                        : Image(
                                            image: AssetImage(
                                                "assets/icons/dotify/playlistsColorLess.png"),
                                            height: 50,
                                            width: 70,
                                          ))),
                          ),
                        ],
                      ),
                    ),

                    //Container with the body
                    Container(
                     
                      height: height * 2/3,
                      child: CustomScrollView(slivers: <Widget>[
                        SliverList(
                          delegate: SliverChildListDelegate([
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child:
                                  Divider(endIndent: 30, color: Colors.black26),
                            )
                          ]),
                        ),
                        model.selectedListId == 0
                            ? SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (BuildContext context, int index) {
                                  return ArtistCard(
                                    onlineArtist:
                                        model.getAvailableArtists()[index],
                                  );
                                },
                                    childCount:
                                        model.getAvailableArtists().length),
                              )
                            : model.selectedListId == 1
                                ? SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                        (BuildContext context, int index) {
                                      return AlbumCard(
                                          onlineAlbum:
                                              model.getfilteredAlbums()[index]);
                                    },
                                        childCount:
                                            model.getfilteredAlbums().length),
                                  )
                                : model.selectedListId == 2
                                    ? SliverList(
                                        delegate: SliverChildBuilderDelegate(
                                            (BuildContext context, int index) {
                                          return SongCard(
                                            song: model
                                                .getAvailableSongs()[index],
                                          );
                                        },
                                            childCount: model
                                                .getAvailableSongs()
                                                .length),
                                      )
                                    : SliverGrid(
                                        delegate: SliverChildBuilderDelegate(
                                            (BuildContext contect, int index) {
                                          return Padding(
                                            padding: EdgeInsets.only(
                                                left: index.isEven ? 10 : 0,
                                                right: index.isOdd ? 10 : 0),
                                            child: PlayListCard(
                                              playlist:
                                                  model.getAvailablePlaylists()[
                                                      index],
                                            ),
                                          );
                                        },
                                            childCount: model
                                                .getAvailablePlaylists()
                                                .length),
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount: 2,
                                                mainAxisSpacing: 1,
                                                crossAxisSpacing: 1,
                                                childAspectRatio: 0.8),
                                      ),
                      ]),
                    )
                  ],
                )),
            bottomNavigationBar: BottomAppBar(
                child: MyBottomAppBar(
              song: model.getAvailableSongs()[model.lastPlayedSongId],
            )));
      },
    );
  }
}
