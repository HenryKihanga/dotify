import 'package:audioplayers/audioplayers.dart';
import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scoped_model/scoped_model.dart';

class NowPlayingPage extends StatefulWidget {
  final OnlineSong song;
  final MainModel model;
  const NowPlayingPage({Key key, @required this.song, @required this.model})
      : super(key: key);

  @override
  _NowPlayingPageState createState() => _NowPlayingPageState();
}

class _NowPlayingPageState extends State<NowPlayingPage> {
  Duration _duration = new Duration();
  Duration _position = new Duration();
  @override
  void initState() {
    initPlayer();
    super.initState();
  }

  void initPlayer() {
    AudioPlayer advancedPlayer = widget.model.audioPlayer;
    advancedPlayer.durationHandler = (d) => setState(() {
          _duration = d;
        });
    advancedPlayer.positionHandler = (p) => setState(() {
          _position = p;
        });
  }

  void seekToSecond(int second) {
    AudioPlayer advancedPlayer = widget.model.audioPlayer;
    Duration newDuration = Duration(seconds: second);
    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
          //backgroundColor: Colors.transparent,
          body: Column(
            children: <Widget>[
              Container(
                height: height / 2,
                decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImageWithRetry(
                                  model.getSongCover(songId: widget.song.id)),
                              fit: BoxFit.cover)),
                child: Stack(
                  children: <Widget>[
                 
                    Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(top: 20, left: 20),
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: Image.asset(
                                "assets/icons/dotify/dismiss_music.png",
                                height: 25,
                              ),
                            ))),
                    Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding: const EdgeInsets.only(top: 20, right: 20),
                            child: IconButton(
                              icon: Image.asset(
                                "assets/icons/dotify/music_info.png",
                                height: 25,
                              ),
                              onPressed: () {
                                print("Info Button");
                              },
                            ))),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Slider(
                          onChanged: (double value) {
                            setState(() {
                              seekToSecond(value.toInt());
                              value = value;
                            });
                          },
                          value: _position.inSeconds.toDouble(),
                          min: 0.0,
                          max: _duration.inSeconds.toDouble(),
                          activeColor: Color(0xffE55F2C),
                          inactiveColor: Colors.grey),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  children: <Widget>[
                    Text(_position.inSeconds.toString()),
                    Spacer(),
                    Text(_duration.inSeconds.toString())
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/favorite_song.png",
                      height: 20,
                    ),
                    onPressed: () {
                      print("Favourite songs");
                    },
                  )),
                  Column(
                    children: <Widget>[
                      Text(widget.song.title),
                      Text("Elon Musk - Take me To The moon"),
                    ],
                  ),
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/more_info.png",
                      height: 20,
                    ),
                    onPressed: () {
                      print("Favourite songs");
                    },
                  ))
                ],
              ),
              SizedBox(
                height: height/15,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/shuffle.png",
                      height: 20,
                    ),
                    onPressed: () {
                      print("shuffle");
                    },
                  )),
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/rewind.png",
                      height: 20,
                    ),
                    onPressed: () {
                      model.play(songId: widget.song.id - 1);
                      model.setIsPlayed(true);
                      print("rewind");
                    },
                  )),
                  Expanded(
                      child: model.isPlayed
                          ? IconButton(
                              iconSize: 70,
                              icon: Image.asset(
                                "assets/icons/dotify/pause.png",
                                color: Colors.black,
                              ),
                              onPressed: () {
                                model.pause();
                                model.setIsPlayed(false);
                              },
                            )
                          : IconButton(
                              iconSize: 70,
                              icon: Image.asset(
                                "assets/icons/dotify/play.png",
                                color: Colors.black,
                              ),
                              onPressed: () {
                                model.play(songId: widget.song.id);
                                model.setIsPlayed(true);
                              },
                            )),
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/fastforward.png",
                      height: 20,
                    ),
                    onPressed: () {
                      model.play(songId: (widget.song.id + 1));
                      model.setIsPlayed(true);
                      print("fast forward");
                    },
                  )),
                  Expanded(
                      child: IconButton(
                    icon: Image.asset(
                      "assets/icons/dotify/repeat.png",
                      height: 20,
                    ),
                    onPressed: () {
                      print("repeat");
                    },
                  ))
                ],
              ),
              SizedBox(
                height: height/15,
              ),
              Container(
                margin: EdgeInsets.only(left: width/3),
                //color: Colors.red,
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(FontAwesomeIcons.volumeUp),
                      onPressed: () {},
                    ),
                    Text("Headphones")
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
