import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/cards/album_card.dart';
import 'package:dotify/views/components/cards/song_card.dart';
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:dotify/views/components/resources/bottom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:scoped_model/scoped_model.dart';

class ArtistPage extends StatelessWidget {
  final OnlineUser onlineArtist;

  const ArtistPage({Key key, @required this.onlineArtist}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            body: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height * 1 / 3,
                      child: Stack(
                        children: <Widget>[
                          Card(
                            elevation: 10,
                            child: Container(
                              height: height * 1 / 3.5,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImageWithRetry(
                                          model.getUserAvatar(
                                              userId: onlineArtist.id)),
                                      fit: BoxFit.cover)),
                            ),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(top: 30, left: 20, right: 35),
                            child: Container(
                              height: height / 16,
                              child: Row(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      print("Got to Your Music");
                                      Navigator.pop(context);
                                    },
                                    child: Image(
                                      image: AssetImage(
                                          "assets/icons/dotify/back_btn_light.png"),
                                      color: Colors.white,
                                      height: 25,
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 15),
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "ARTIST",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            onlineArtist.name,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.white),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: CircleAvatar(
                              backgroundImage: NetworkImageWithRetry(
                                  model.getUserAvatar(userId: onlineArtist.id)),
                              radius: 55,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 200, left: 125),
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        "assets/icons/dotify/heart_song.png"),
                                    height: 40,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Image(
                                    image: AssetImage(
                                        "assets/icons/dotify/play_artist.png"),
                                    height: 60,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Image(
                                    image: AssetImage(
                                        "assets/icons/dotify/share.png"),
                                    height: 40,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      height: height * 1 / 1.8,
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: CustomScrollView(
                          slivers: <Widget>[
                            SliverList(
                              delegate: SliverChildListDelegate([
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text("LATEST RELEASE"),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Divider(
                                    color: Colors.black26,
                                    endIndent: 30,
                                  ),
                                ),
                              ]),
                            ),
                            SliverList(
                                   delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return AlbumCard(
                                      onlineAlbum: onlineArtist.albums[index]);
                              }, childCount: onlineArtist.albums.length),

                            //     delegate: SliverChildListDelegate([
                            //       //display latest released album
                            //   AlbumCard(
                            //       onlineArtist: onlineArtist,
                            //       onlineAlbum: onlineArtist
                            //           .albums[onlineArtist.albums.length - 1])
                            // ])
                            ),
                            SliverList(
                              delegate: SliverChildListDelegate([
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Text("POPULAR"),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Divider(
                                    color: Colors.black,
                                    endIndent: 30,
                                  ),
                                ),
                              ]),
                            ),
                            SliverList(
                              delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                return SongCard(
                                    song: onlineArtist.songs[index]);
                              }, childCount: onlineArtist.songs.length),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              child: MyBottomAppBar(
                song: model.getAvailableSongs()[model.lastPlayedSongId],
              ),
            ));
      },
    );
  }
}
