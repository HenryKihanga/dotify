import 'package:dotify/constants/constant.dart';
import 'package:dotify/views/components/resources/background.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class SignupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return BackGround(
      sigmaX: 15,
      sigmaY: 15,
      scaffold: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Center(
              child: Image(
            image: AssetImage("assets/icons/dotify/dotify_icon.png"),
            height: 35,
          )),
          leading: IconButton(
                onPressed: () {
                  print("back to launch screen");
                  Navigator.pushNamed(context, launchPage);
                },
                icon: Image.asset(
                  "assets/icons/dotify/back.png",
                  height: height / 35,
                ),
              )
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(delegate: SliverChildListDelegate([
              Column(
          children: <Widget>[
            Container(
              height: height / 15,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton.icon(
                      icon: Icon(
                        FontAwesomeIcons.facebookF,
                        color: Colors.white,
                        size: 30,
                      ),
                      label: Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Sign up with Facebook",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      color: Color(0xff334F8D),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text("or with email"),
            ),
            Form(
              child: Column(
                children: <Widget>[
                  Card(
                    child: TextFormField(
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: InputBorder.none,
                          hintText: 'Email',
                          prefixIcon: Icon(
                            Icons.email,
                            color: Colors.black,
                          )),
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: InputBorder.none,
                          hintText: 'Choose username',
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.black,
                          )),
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: InputBorder.none,
                          hintText: ' Choose password',
                          prefixIcon: Icon(
                            Icons.lock,
                            color: Colors.black,
                          )),
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: InputBorder.none,
                          hintText: 'Date of birth',
                          prefixIcon: Icon(
                            Icons.calendar_today,
                            color: Colors.black,
                          )),
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          border: InputBorder.none,
                          hintText: 'Gender',
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.black,
                          )),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
            ]),)
          ],
        ),
        
        bottomNavigationBar: BottomAppBar(
            child: Container(
          height: height / 15,
          child: FlatButton(
            color: Color(0xffE55F2C),
            child: Text(
              "SIGN UP",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
            onPressed: () {},
          ),
        )),
      ),
    );
  }
}
