import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/components/cards/song_card.dart';
import 'package:dotify/views/components/models/online_album_model.dart';
import 'package:dotify/views/components/resources/bottom_app_bar.dart';
import 'package:dotify/views/components/resources/screen_cover.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AlbumPage extends StatelessWidget {
  final OnlineAlbum onlineAlbum;
  

  const AlbumPage({Key key, @required this.onlineAlbum, })
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            body: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    //calling the screen cover
                    ScreenCover(
                      screenName: onlineAlbum.name,
                      screenCover: model.getAlbumCover(
                        albumId: onlineAlbum.id,
                      ),
                      screenAvatar: model.getAlbumCover(
                        albumId: onlineAlbum.id,
                      ),
                      screentitle: "ALBUM",
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Divider(
                          color: Colors.black26,
                          endIndent: 20,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      height: height * 1 / 2,
                      child: CustomScrollView(
                        slivers: <Widget>[
                          SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                              return  
                              SongCard(
                                song: onlineAlbum.songs[index],
                              );
                            }, childCount: onlineAlbum.songs.length),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: BottomAppBar(
                child: MyBottomAppBar(
              song: model.getAvailableSongs()[model.lastPlayedSongId],
            )));
      },
    );
  }
}
