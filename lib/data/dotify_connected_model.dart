import 'dart:convert';
import 'package:audioplayers/audioplayers.dart';
import 'package:dotify/constants/api.dart';
import 'package:dotify/views/components/models/online_album_model.dart';
import 'package:dotify/views/components/models/online_playlist_model.dart';
import 'package:dotify/views/components/models/online_role.dart';
import 'package:dotify/views/components/models/online_song_model.dart';
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/subjects.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

mixin DotifyConnecterModel on Model {
//for SongModel
  int _lastPlayedSongId = 0;
  bool _isPlayed = false;

//your music page
  int _selectedListId = 2;
  bool _isArtistTapped = false;
  bool _isAlbumsTapped = false;
  bool _isSongsTapped = true;
  bool _isPlayListTapped = false;

//album model
  List<OnlineAlbum> _availableAlbums;
  List<OnlineAlbum> _filteredAlbums;

//song model
  List<OnlineSong> _availableSongs;

//user model
  List<OnlineUser> _availableUsers;
  List<OnlineUser> _availableArtists;

//playlist model
  List<OnlinePlaylist> _availablePlaylists;
  int _idOfSongToAdd;

//utility model
  bool _addplayList = false;

  // int _selectedPlayListId;

  // List<Song> _songs;
  // List<Song> _playList = [
  //   Song(
  //       id: 1,
  //       cover: "",
  //       albumId: 4,
  //       name: "fgvhv",
  //       views: "65767",
  //       playListId: 1)
  // ];
  // List<Song> _filteredPlayList;

  // bool _showBottomBar = false;
  //
  // Song _musicToAddInPlayList;
  //
  // bool _addplayList = false;

  //
}

mixin YourMusicPageModel on DotifyConnecterModel {
/*set the index of current PlayList picked 
  To be used to loop playLists in PlayList Screen
*/
  void setCurrentListId({@required int listId}) {
    _selectedListId = listId;
    listId == 0 ? _isArtistTapped = true : _isArtistTapped = false;

    listId == 1 ? _isAlbumsTapped = true : _isAlbumsTapped = false;

    listId == 2 ? _isSongsTapped = true : _isSongsTapped = false;

    listId == 3 ? _isPlayListTapped = true : _isPlayListTapped = false;
    notifyListeners();
  }

//get the id of list button
  int get selectedListId => _selectedListId;

//get the tapped button
  bool get isAlbumTapped => _isAlbumsTapped;
  bool get isArtistTapped => _isArtistTapped;
  bool get isSongsTapped => _isSongsTapped;
  bool get isPlayListTapped => _isPlayListTapped;
}

mixin AlbumModel on DotifyConnecterModel {
  Future<void> fetchAllAlbums() async {
    final List<OnlineAlbum> _fetchedAlbums = [];
    List<OnlineAlbum> _filteredAlbum = [];

    try {
      final http.Response response = await http.get(api + 'albums');
      Map<String, dynamic> data = json.decode(response.body);

      if (response.statusCode == 200) {
        //decode album from data map
        data['albums'].forEach((albumData) {
          final OnlineAlbum _album = OnlineAlbum.fromMap(albumData);
          //decode song from decoded album
          // albumData['songs'].forEach((songData) {
          //   final OnlineSong _song = OnlineSong.fromMap(songData);
          //   _album.songs.add(_song); //adding songs into album songs list
          // });

          _fetchedAlbums.add(_album); //adding fetched album into list of albums

          //fetch albums which  are not default
          final _fetchedNonDefaultAlbums =
              _fetchedAlbums.where((album) => album.isDefault == false);

          //fetch albums which are neither default nor empty
          final _fetchedPureAlbums =
              _fetchedNonDefaultAlbums.where((album) => album.songs.isNotEmpty);

          //decode fetched albums from iterative array
          _filteredAlbum = List<OnlineAlbum>.from(_fetchedPureAlbums);
        });
      } else {
        print(response.statusCode);
      }
    } catch (error) {
      print(error);
    }

    _availableAlbums = _fetchedAlbums; //all albums
    _filteredAlbums = _filteredAlbum; //neither default nor empty albums

    notifyListeners();
  }

//getter
  List<OnlineAlbum> getAvailableAlbums() {
    if (_availableAlbums == null) {
      return <OnlineAlbum>[];
    }

    return _availableAlbums;
  }

//getter
  List<OnlineAlbum> getfilteredAlbums() {
    if (_filteredAlbums == null) {
      return <OnlineAlbum>[];
    }

    return _filteredAlbums;
  }

//method to get album cover
  String getAlbumCover({@required int albumId}) {
    return api + 'album/viewAlbumCover/' + albumId.toString();
  }
}

mixin SongModel on DotifyConnecterModel {
  Future<void> fetchAllSongs() async {
    final List<OnlineSong> _fetchedSongs = [];

    try {
      final http.Response response = await http.get(api + 'songs');
      final Map<String, dynamic> data = json.decode(response.body);
      if (response.statusCode == 200) {
        data['songs'].forEach((songData) {
          final OnlineSong _song = OnlineSong.fromMap(songData);
          _fetchedSongs.add(_song);
        });
      } else {
        print(response.statusCode);
      }
    } catch (error) {
      print(error);
    }

    _availableSongs = _fetchedSongs;
    notifyListeners();
  }

//getter
  List<OnlineSong> getAvailableSongs() {
    if (_availableSongs == null) {
      return <OnlineSong>[];
    }
    return _availableSongs;
  }

//method to add song into playlist

//method to play song
  AudioPlayer audioPlayer = AudioPlayer();
  play({@required songId}) async {
    int result =
        await audioPlayer.play(api + 'song/viewSong/' + songId.toString());
    if (result == 1) {
      // success
    }
  }

//method to pause song
  pause() async {
    int result = await audioPlayer.pause();
    if (result == 1) {
      // success
    }
  }

//  //method to play all songs
//  playAll(songId) async {
//    int result =
//         await audioPlayer.play(api + 'song/viewSong/' + songId.toString());
//      audioPlayer.onPlayerCompletion.listen((event)  {
//         songId++;
//         notifyListeners();

//   });
//  }

//method to set id of playing song
  void setNowPlayingSongId({@required int songId}) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setInt('lastSongId', (songId - 1));
    notifyListeners();
    _lastPlayedSongId = pref.getInt('lastSongId');
  }

//getter
  int get lastPlayedSongId => _lastPlayedSongId;

//method to get song cover
  String getSongCover({@required int songId}) {
    return api + 'song/viewSongCover/' + songId.toString();
  }

//method to set if song is played
  void setIsPlayed(bool play) {
    _isPlayed = play;
    notifyListeners();
  }

//getter
  bool get isPlayed => _isPlayed;
}

mixin UserModel on DotifyConnecterModel {
  Future<void> fetchAllUsers() async {
    final List<OnlineUser> _fetchedUsers = [];
    List<OnlineUser> _fetchedArtists = [];
    final List<OnlineAlbum> _fetchedUserAlbums = [];

    try {
      final http.Response response = await http.get(api + 'users');
      final Map<String, dynamic> data = json.decode(response.body);

      if (response.statusCode == 200) {
        //decode user from map
        data['users'].forEach((userData) {
          final OnlineUser _user = OnlineUser.fromMap(userData);
          //decode user album from map
          userData['albums'].forEach((albumData) {
            final OnlineAlbum _album = OnlineAlbum.fromMap(albumData);
            _fetchedUserAlbums.add(_album);

            //fetch albums which  are not default
            final _fetchedNonDefaultAlbums =
                _fetchedUserAlbums.where((album) => album.isDefault == false);

            //fetch albums which have songs or which are not default
            final _fetchedPureAlbums = _fetchedNonDefaultAlbums
                .where((album) => album.songs.isNotEmpty);

            //decode fetched albums
            List<OnlineAlbum> _filterdAlbum = [];
            _filterdAlbum = List<OnlineAlbum>.from(_fetchedPureAlbums);
            _user.albums.add(_album); //add to user album

            //decode useralbum songs from map
            albumData['songs'].forEach((songData) {
              final OnlineSong _song = OnlineSong.fromMap(songData);
              _album.songs.add(_song);
            });
          });

          //decode user songs from map
          userData['songs'].forEach((songData) {
            final OnlineSong _song = OnlineSong.fromMap(songData);
            _user.songs.add(_song); //adding songs related to user
          });
          //decode user roles from map
          userData['roles'].forEach((roleData) {
            final OnlineRole _role = OnlineRole.frommap(roleData);
            _user.roles = _role; //adding roles related to user
          });

          _fetchedUsers.add(_user); //add user into list
        });
      } else {
        print(response.statusCode);
      }
    } catch (error) {
      print(error);
    }
    _availableUsers = _fetchedUsers;

    final _artistUsers =
        _availableUsers.where((user) => user.roles.name == 'Artist');
    _fetchedArtists = List<OnlineUser>.from(_artistUsers);
    _availableArtists = _fetchedArtists;

    notifyListeners();
  }

  List<OnlineUser> getAvailableUsers() {
    if (_availableUsers == null) {
      return <OnlineUser>[];
    }
    return _availableUsers;
  }

  List<OnlineUser> getAvailableArtists() {
    if (_availableArtists == null) {
      return <OnlineUser>[];
    }
    return _availableArtists;
  }

  //method to get user avatar
  String getUserAvatar({@required int userId}) {
    return api + 'user/viewUserAvatar/' + userId.toString();
  }
}

mixin Authentication on DotifyConnecterModel {
  PublishSubject<bool> _userSubject =
      PublishSubject(); //capture if user authenticated succesfully
  OnlineUser _authenticatedUser;

  // getters
  OnlineUser get authenticatedUser => _authenticatedUser;
  PublishSubject<bool> get userSubject => _userSubject;

  // login
  Future<bool> login({@required String name, @required String password}) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    OnlineUser _user;
    bool _isLogedIn = false;
    //map user inputs
    Map<String, dynamic> authData = {'name': name, 'password': password};
    //encode user input into json
    final body = json.encode(authData);
    //post the user inputs to the server
    try {
      http.Response response = await http.post(api + 'login',
          body: body, headers: {'Content-Type': 'application/json'});
      //decode from json the post response body
      Map<String, dynamic> data = json.decode(response.body);

      //check if response data contains token then get the user data
      if (data.containsKey('token')) {
        final OnlineUser _userData =
            OnlineUser.fromMap(data); //unmap obtained user data
        _user = _userData;

        _pref.setString('authenticatedUser',
            response.body); //store user data in mobile phone
        _userSubject.add(true);
        _isLogedIn = true;
      } else {
        print(data['error']);
        _userSubject.add(false);
      }
    } catch (e) {
      print(e);
    }
    _authenticatedUser = _user;
    notifyListeners();
    return _isLogedIn;
  }

  // logout
  Future<void> logout({@required String token}) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    Map<String, dynamic> data;

    final body = json.encode({'token': token});
    try {
      http.Response response = await http.post(api + 'logout',
          body: body, headers: {'Content-Type': 'application/json'});
      data = json.decode(response.body);
      if (data.containsKey('message')) {
        _userSubject.add(false);
      }
    } catch (e) {
      print(e);
    }
    print(data);
    _pref.clear();
   
  }

  // register
  Future<void> registerUser(
      {String name, String email, String password}) async {
    OnlineUser _user;

    Map<String, dynamic> signUpData = {
      'name': name,
      'email': email,
      'password': password
    };
    try {
      http.Response response = await http.post(api + 'register',
          body: signUpData, headers: {'Content-Type': 'application/json'});
      Map<String, dynamic> data = json.decode(response.body);

      if (data.containsKey('token')) {
        final OnlineUser _userData =
            OnlineUser.fromMap(data); //unmap obtained user data
        _user = _userData;
        _userSubject.add(true);
      } else {
        _userSubject.add(false);
      }
    } catch (e) {
      print('error: $e');
    }
    _authenticatedUser = _user;
    notifyListeners();
  }

  Future<void> autoAuthenticate() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    String _logedInUser = _pref.getString('authenticatedUser');
    OnlineUser _user;

    Map<String, dynamic> data = json.decode(_logedInUser);

    if (data.containsKey('token')) {
      final OnlineUser _userData = OnlineUser.fromMap(data);
      _user = _userData;
      _authenticatedUser = _user;
      _userSubject.add(true);
    } else {
      _userSubject.add(false);
    }

    notifyListeners();
  }
}

// mixin AlbumScreenModel on DotifyConnecterModel {
// /*set the index of current Album picked
//   To be used to loop albums in Album Screen
// */
//

//

// /*=======================xxxxxxxxx==============xxxxxx==========xxxxxx=======*/

// //method to fetch songs by related to given album
//   void fetchAlbumSongs({@required int albumId}) {
//     final fetchedsongs = songs.where((songs) => songs.albumId == albumId);
//     _songs = List<Song>.from(fetchedsongs);
//     notifyListeners();
//   }

//   //get the songs related to the album
//   List<Song> getfetchedAlbumSongs() {
//     if (_songs == null) {
//       return <Song>[];
//     }
//     return List<Song>.from(_songs);
//   }
// }

// mixin PlayListsScreenModel on DotifyConnecterModel {
// /*set the index of current PlayList picked
//   To be used to loop playLists in PlayList Screen
// */
//   void setCurrentPlayListId({@required int playListId}) {
//     _selectedPlayListId = playListId;
//     notifyListeners();
//   }

// //get the index of current PlayList picked
//   int get selectedPlayListId => _selectedPlayListId;

// /*=======================xxxxxxxxx==============xxxxxx==========xxxxxx=======*/

//

// /*=======================xxxxxxxxx==============xxxxxx==========xxxxxx=======*/

// //method to check the songs for being added into corresponding playlist
//   void findMusicToAdd({@required int musicId, @required int playListId}) {
//     final index = songs.indexWhere((songs) => songs.id == musicId);
//     _musicToAddInPlayList = Song(
//       id: songs[index].id,
//       albumId: songs[index].albumId,
//       name: songs[index].name,
//       cover: songs[index].cover,
//       views: songs[index].views,
//       playListId: playListId,
//     );
//     _playList.add(_musicToAddInPlayList);
//     notifyListeners();
//   }

//   //method to filter which song belongs to which playlist
//   void filterPLayList({@required int playListId}) {
//     final fetchedPlayList =
//         _playList.where((playlist) => playlist.playListId == playListId);

//     _filteredPlayList = List<Song>.from(fetchedPlayList);

//     notifyListeners();
//   }

//   //get filtered playlist songs
//   List<Song> getfilteredPlayList() {
//     if (_filteredPlayList == null) {
//       return <Song>[];
//     }
//     return List<Song>.from(_filteredPlayList);
//   }
// /*=======================xxxxxxxxx==============xxxxxx==========xxxxxx=======*/

// }

mixin PlayListModel on DotifyConnecterModel {
  Future<void> fetchAllPlaylists() async {
    final List<OnlinePlaylist> _fetchedPlaylist = [];

    try {
      final http.Response response = await http.get(api + 'playlists');
      final Map<String, dynamic> data = json.decode(response.body);
      if (response.statusCode == 200) {
        data['playlists'].forEach((playlistData) {
          final OnlinePlaylist _playlist = OnlinePlaylist.fromMap(playlistData);
          playlistData['songs'].forEach((songData) {
            final OnlineSong _song = OnlineSong.fromMap(songData);
            _playlist.songs.add(_song);
          });

          _fetchedPlaylist.add(_playlist);
        });
      } else {
        print(response.statusCode);
      }
    } catch (error) {
      print(error);
    }

    _availablePlaylists = _fetchedPlaylist;

    notifyListeners();
  }

//getter
  List<OnlinePlaylist> getAvailablePlaylists() {
    if (_availablePlaylists == null) {
      return <OnlinePlaylist>[];
    }
    return _availablePlaylists;
  }

//playlist cover
  String getPlaylistCover({@required int playlistId}) {
    return api + 'playlist/viewCover/' + playlistId.toString();
  }

//method to get id of songs required to be addred in playlist
  void idOfsongToAdd({@required int songId}) {
    _idOfSongToAdd = songId;
    notifyListeners();
  }

//getter
  int get idOfSongToAdd => _idOfSongToAdd;

//method to add song into playlist
  addToPlaylist(int songId, int playlistId) async {
    http.post(api +
        'addSongtoPlaylist/' +
        songId.toString() +
        '/' +
        playlistId.toString());
  }
}


mixin UtilityModel on DotifyConnecterModel {
  void doAddSongToPlaylist() {
    _addplayList = !_addplayList;
    notifyListeners();
  }

  bool get addSongToPlayList => _addplayList;
  // void showBottomAppBar() {
  //   _showBottomBar = true;
  //   notifyListeners();
  // }

  // bool get showBottomBar => _showBottomBar;
}
