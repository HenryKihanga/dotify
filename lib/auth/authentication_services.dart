
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:flutter/material.dart';

abstract class AuthenticationService {
  Future<OnlineUser> registerUser({@required String name, @required String email, @required String password});
  Future<OnlineUser> login({@required String name, @required String password});
  Future<String> logout({@required String token});

  void dispose();
}