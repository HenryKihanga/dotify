import 'dart:convert';
import 'package:dotify/auth/authentication_services.dart';
import 'package:dotify/constants/api.dart';
import 'package:dotify/views/components/models/online_user_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LaravelAuthentication implements AuthenticationService {
  LaravelAuthentication();
  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  Future<OnlineUser> login(
      {@required String name, @required String password}) async {
    OnlineUser _user;
    Map<String, dynamic> authData = {'name': name, 'password': password};
    final body = json.encode(authData);
    try {
      http.Response response = await http.post(api + 'login',
          body: body, headers: {'Content-Type': 'application/json'});
      Map<String, dynamic> data = json.decode(response.body);
      if (data.containsKey('token')) {
        _user = OnlineUser(
          id: data['user']['id'],
          name: data['user']['name'],
          email: data['user']['email'],
          roles: data['roles'],
          albums: data['albums'],
          token: data['token'],
        );
      }
    } catch (e) {
      print(e);
    }
    return _user;
  }

  @override
  Future<String> logout({String token}) async {
    Map<String, dynamic> data;
    try {
      http.Response response = await http.post(api + 'logout',
          body: {'token': token},
          headers: {'Content-Type': 'application/json'});
      data = json.decode(response.body);
    } catch (e) {}
    return data['message'];
  }

  @override
  Future<OnlineUser> registerUser(
      {String name, String email, String password}) async {
    OnlineUser _user;
    Map<String, dynamic> signUpData = {
      'name': name,
      'email': email,
      'password': password
    };
    try {
      http.Response response = await http.post(api + 'register_user',
          body: signUpData, headers: {'Content-Type': 'application/json'});
      Map<String, dynamic> data = json.decode(response.body);
      _user = OnlineUser(
        id: data['user']['id'],
        name: data['user']['name'],
        email: data['user']['email'],
        roles: data['roles'],
        albums: data['albums'],
        token: data['token'],
      );
    } catch (e) {
      print('error: $e');
    }
    return _user;
  }
}
