import 'package:dotify/constants/constant.dart';
import 'package:dotify/data/main_model.dart';
import 'package:dotify/views/pages/browse_page.dart';
import 'package:dotify/views/pages/drawer_page.dart';
import 'package:dotify/views/pages/launch_page.dart';
import 'package:dotify/views/pages/login_page.dart';
import 'package:dotify/views/pages/signup_page.dart';
import 'package:dotify/views/pages/your_music_page.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final MainModel _model = MainModel();
  bool _isAuthenticated = false;
  @override
  void initState() {
    _model.autoAuthenticate();
    _model.fetchAllSongs();
    _model.userSubject.listen((value) {
      setState(() {
        _isAuthenticated = value;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel(
      model: _model,
      child: MaterialApp(
        title: 'dotify',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: {
          weird: (BuildContext context) =>
              _isAuthenticated ? BrowsePage() : LaunchPage(),
          launchPage: (BuildContext context) => LaunchPage(),
          loginPage: (BuildContext context) => LoginPage(),
          signupPage: (BuildContext context) => SignupPage(),
          browsePage: (BuildContext context) => BrowsePage(),
          drawerPage: (BuildContext context) => DrawerPage(),
          yourMusicPage: (BuildContext context) => YourMusicPage(
                model: _model,
              ),
        },
      ),
    );
  }
}
